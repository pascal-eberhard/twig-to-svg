<?php
declare(strict_types=1);

/*
 * This file is part of the twig-to-svg package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhard\TwigToSvg\Tests\Io\Info\PathString;

use PascalEberhard\TwigToSvg\Io\Info\PathString\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PascalEberhard\TwigToSvg\Io\Info\PathString\Utils
 *
 * Shell: (bin/phpunit tests/Io/Info/PathString/UtilsTest.php)
 */
class UtilsTest extends TestCase
{

    /**
     * @see self::testIsWindowsFormat
     */
    public function dataIsWindowsFormat() {
        return [
            /**
             * @param bool $result Expected method result
             * @param string $path Must be absolute
             *
             * @todo Check for absolute path @ ::isWindowsFormat
             */
            [false, '/'],
            [false, '\\'],
            [false, '/c/'],
            [false, '\\c:\\'],
            [true, 'c:\\'],
            [true, 'c:/'],
        ];
    }

    /**
     * @see self::testLinuxToWindows
     */
    public function dataLinuxToWindows() {
        return [
            /**
             * @param string $result Converted path
             * @param string $path Must be absolute
             *
             * @todo Check for absolute path @ ::windowsToLinux
             * Hint: The drive letter is always converted to upper case
             */
            ['C:\\windows', '/c/windows'],
            ['C:\\Windows', '/c/Windows'],
            ['C:\\windows\\', '/c/windows/'],
            ['C:\\Windows\\', '/c/Windows/'],
            ['path', 'path'],
        ];
    }

    /**
     * @see self::testWindowsToLinux
     */
    public function dataWindowsToLinux() {
        return [
            /**
             * @param string $result Converted path
             * @param string $path Must be absolute
             *
             * @todo Check for absolute path @ ::windowsToLinux
             * Hint: The drive letter is always converted to lower case
             */
            ['/c/windows', 'C:\\windows'],
            ['/c/Windows', 'c:\\Windows'],
            ['/c/windows/', 'C:\\windows\\'],
            ['/c/Windows/', 'c:\\Windows\\'],
            ['path', 'path'],
        ];
    }

    /**
     * @covers ::isWindowsFormat
     * @dataProvider dataIsWindowsFormat
     *
     * @param bool $result Expected method result
     * @param string $path Must be absolute
     * Shell: (bin/phpunit tests/Io/Info/PathString/UtilsTest.php --filter testIsWindowsFormat)
     */
    public function testIsWindowsFormat(bool $result, string $path)
    {
        $this->assertEquals($result, Utils::isWindowsFormat($path));
    }

    /**
     * @covers ::linuxToWindows
     * @dataProvider dataLinuxToWindows
     *
     * @param string $result Converted path
     * @param string $path Must be absolute
     * Shell: (bin/phpunit tests/Io/Info/PathString/UtilsTest.php --filter testLinuxToWindows)
     */
    public function testLinuxToWindows(string $result, string $path)
    {
        $this->assertEquals($result, Utils::linuxToWindows($path));
    }

    /**
     * @covers ::windowsToLinux
     * @dataProvider dataWindowsToLinux
     *
     * @param string $result Converted path
     * @param string $path Must be absolute
     * Shell: (bin/phpunit tests/Io/Info/PathString/UtilsTest.php --filter testWindowsToLinux)
     */
    public function testWindowsToLinux(string $result, string $path)
    {
        $this->assertEquals($result, Utils::windowsToLinux($path));
    }
}
