<?php
declare(strict_types=1);

/*
 * This file is part of the twig-to-svg package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhard\TwigToSvg;

use PascalEberhard\TwigToSvg\Charset;
use PascalEberhard\TwigToSvg\Io\Info\PathString;

/**
 * Config data
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Config
{
    
    /**
     * Default charset
     *
     * @var string
     */
    const CHARSET = Charset\Enum::UTF_8;

    /**
     * Default directory path separator
     *
     * @var string
     */
    const DIRECTORY_SEPARATOR = '/';

    /**
     * Base dir
     *
     * @var string
     */
    private static $baseDir = '';

    /**
     * Base dir set?
     *
     * @var bool
     */
    private static $baseDirSet = false;

    /**
     * Base dir, windows format
     *
     * @var string
     */
    private static $baseDirWindows = '';

    /**
     * Get base dir
     *
     * @return string
     * @throws \UnexpectedValueException
     */
    public static function baseDir(): string
    {
        if (self::$baseDirSet) {
            return self::$baseDir;
        }

        self::$baseDir = PathString\Utils::windowsToLinux(__FILE__);
        if ('/src/Config.php' !== mb_substr(self::$baseDir, -15, 15, Config::CHARSET)) {
            throw new \UnexpectedValueException('Unexpected file path: ' . self::$baseDir);
        }

        self::$baseDir = mb_substr(self::$baseDir, 0, -14, Config::CHARSET);
        self::$baseDirSet = true;
        self::$baseDirWindows = PathString\Utils::linuxToWindows(self::$baseDir);

        return self::$baseDir;
    }

    /**
     * Get base dir, windows format
     *
     * @return string
     */
    public static function baseDirWindows(): string
    {
        if (self::$baseDirSet) {
            return self::$baseDirWindows;
        }

        self::baseDir();

        return self::$baseDirWindows;
    }
}
