<?php
declare(strict_types=1);

/*
 * This file is part of the twig-to-svg package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhard\TwigToSvg\Charset;

use MyCLabs\Enum as BaseEnum;

/**
 * @example UTF-8
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Enum extends BaseEnum\Enum
{

    /**
     * UTF-8
     *
     * @var string
     */
    const UTF_8 = 'utf8';
}
