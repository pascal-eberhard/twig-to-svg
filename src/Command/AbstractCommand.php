<?php

/*
 * This file is part of the twig-to-svg package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PascalEberhard\TwigToSvg\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Some additions to \Symfony\Component\Console\Command\Command
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2018 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
abstract class AbstractCommand extends Command
{

    /**
     * Error exit code
     *
     * @var int
     */
    const EXIT_ERROR = 1;

    /**
     * Success exit code
     *
     * @var int
     */
    const EXIT_SUCCESS = 0;

    /**
     * Input interface
     *
     * @var \Symfony\Component\Console\Input\InputInterface|null
     */
    private $input = null;

    /**
     * Output interface
     *
     * @var \Symfony\Component\Console\Output\OutputInterface|null
     */
    private $output = null;

    /**
     * Get input interface
     *
     * @return \Symfony\Component\Console\Input\InputInterface
     * @throws \LogicException If input interface not set
     * @see \PascalEberhard\TwigToSvg\Command\AbstractCommand::setInput()
     */
    protected function getInput(): InputInterface
    {
        if (null === $this->input) {
            throw new \LogicException('Input interface not set, use ::setInput()');
        }
        
        return $this->input;
    }

    /**
     * Get output interface
     *
     * @return \Symfony\Component\Console\Output\OutputInterface
     * @throws \LogicException If output interface not set
     * @see \PascalEberhard\TwigToSvg\Command\AbstractCommand::setOutput()
     */
    protected function getOutput(): OutputInterface
    {
        if (null === $this->output) {
            throw new \LogicException('Output interface not set, use ::setOutput()');
        }
        
        return $this->output;
    }

    /**
     * Set input interface
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input
     */
    protected function setInput(InputInterface $input)
    {
        $this->input = $input;
    }

    /**
     * Set output interface
     *
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }
}
