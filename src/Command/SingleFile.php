<?php

/*
 * This file is part of the twig-to-svg package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PascalEberhard\TwigToSvg\Command;

use PascalEberhard\TwigToSvg\Config as MyConfig;
use PascalEberhard\TwigToSvg\Io\Info\PathString\Utils;
use Symfony\Component\Config;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use Twig;

/**
 * Generate SVG from template
 * Generate single file, command
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2018 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * Shell: (php bin/project/svgGenerator.php single.file config/singleFile.yml -vv)
 */
class SingleFile extends AbstractCommand
{

    /**
     * Config file path parameter
     *
     * @var string
     */
    const ARGUMENT_CONFIG_FILE = 'config-file';

    /**
     * Command
     *
     * @var string
     */
    const COMMAND = 'single.file';

    /**
     * Config data
     *
     * @var array
     */
    protected $config = [];

    protected function configure()
    {
        $this
            ->setName(self::COMMAND)
            ->setDescription('Generate single file (SVG) via twig template from config file')
            ->setHelp('Generate single file (SVG) via twig template from config file')

            ->addArgument(
                self::ARGUMENT_CONFIG_FILE,
                InputArgument::REQUIRED,
                'Path to YAML config file. Relative path is prefixed with project directory.'
            )
        ;
    }

    protected function doTemplate()
    {
        $loader = new Twig\Loader\FilesystemLoader(Utils::linuxToWindows(dirname($this->config['template'])));
        $twig = new Twig\Environment($loader, [
            'cache' => false,
            #'charset' => MyConfig::CHARSET,
        ]);

        file_put_contents(
            Utils::linuxToWindows($this->config['output']),
            $twig->render(basename($this->config['template']), $this->config['data'])
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int Exit code
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->config = $this->getConfig($input->getArgument(self::ARGUMENT_CONFIG_FILE));
        $this->doTemplate();
        $output->writeln('Output to: ' . $this->config['output']);

        return self::EXIT_SUCCESS;
    }

    /**
     * Get config data
     *
     * @param string $file File path to config file
     * @return array
     * @see \InvalidArgumentException
     */
    protected function getConfig(string $file): array
    {
        // Checks and init stuff
        if ('' === $file) {
            throw new \InvalidArgumentException('$file must not be empty');
        } elseif (!is_file($file)) {
            $file = MyConfig::baseDir() . $file;
        }

        if (!is_file($file)) {
            throw new \InvalidArgumentException('$file not found: ' . $file);
        }

        $data = (new Config\Definition\Processor())->processConfiguration(
            new ConfigValidation(),
            Yaml::parseFile($file)
        );

        return $data;
    }
}
