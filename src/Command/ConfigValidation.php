<?php

/*
 * This file is part of the twig-to-svg package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PascalEberhard\TwigToSvg\Command;

use PascalEberhard\TwigToSvg\Config as MyConfig;
use PascalEberhard\TwigToSvg\Io\Info\PathString;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2018 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class ConfigValidation implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('config');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('data')
                    ->children()
                        ->arrayNode('color')
                            ->children()
                                ->scalarNode('background')->cannotBeEmpty()->beforeNormalization()->always(
                                    function ($v) {
                                        return self::validateRgbColor($v, 'Config.data.color.background');
                                    }
                                )->end()->end()
                                ->scalarNode('foreground')->cannotBeEmpty()->beforeNormalization()->always(
                                    function ($v) {
                                        return self::validateRgbColor($v, 'Config.data.color.foreground');
                                    }
                                )->end()->end()
                            ->end()
                        ->end()
                        ->scalarNode('text')->cannotBeEmpty()->end()
                    ->end()
                ->end()
                ->scalarNode('mode')->cannotBeEmpty()->end()
                ->scalarNode('output')->cannotBeEmpty()->beforeNormalization()->always(
                    function ($v) {
                        return self::validateFilePath($v, 'Config.output', false, 'svg');
                    }
                )->end()->end()
                ->scalarNode('template')->cannotBeEmpty()->beforeNormalization()->always(
                    function ($v) {
                        return self::validateFilePath($v, 'Config.template', true, 'twig');
                    }
                )->end()->end()
            ->end()
        ;

        return $treeBuilder;
    }

    /**
     * Validate file path
     *
     * @param mixed $value
     * @param string $key Config key, for errorr messages
     * @param bool $mustExist
     * @param string $ext Optional, allowed file extension
     * @return string
     * @see \LogicException
     */
    private static function validateFilePath($value, string $key, bool $mustExist, string $ext = ''): string
    {
        if (!is_string($value)) {
            throw new \LogicException($key . ' must be string, is ' . gettype($value));
        } elseif ('' === $value) {
            throw new \LogicException($key . ' must not be empty');
        }

        $value = PathString\Utils::windowsToLinux($value);
        if (0 !== mb_strpos($value, MyConfig::baseDir(), 0, MyConfig::CHARSET)) {
            $value = MyConfig::baseDir() . $value;
        }

        if ($mustExist && !is_file(PathString\Utils::linuxToWindows($value))) {
            throw new \LogicException($key . ' file not found: ' . $value);
        }

        if ('' !== $ext) {
            $thisExt = pathinfo($value, PATHINFO_EXTENSION);
            if ($ext !== $thisExt) {
                throw new \LogicException($key . ' file type wrong, not (' . $ext . ')not found: ' . $value);
            }
        }

        return $value;
    }

    /**
     * Validate RGB color hex value
     *
     * @param mixed $value
     * @param string $key Config key, for errorr messages
     * @return string
     * @see \LogicException
     */
    private static function validateRgbColor($value, string $key): string
    {
        if (!is_string($value)) {
            throw new \LogicException($key . ' must be string, is ' . gettype($value));
        } elseif ('' === $value) {
            throw new \LogicException($key . ' must not be empty');
        } elseif (!ctype_alnum($value)) {
            throw new \LogicException($key . ' must only contain alpha numeric chars: ' . $value);
        } elseif (0 === preg_match('/^[0-9a-f]{6}$/iu', $value)) {
            throw new \LogicException($key . ' must be hexadecimal RGB color value: ' . $value);
        }

        return $value;
    }
}
