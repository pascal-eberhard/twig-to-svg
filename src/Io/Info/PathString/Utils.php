<?php
declare(strict_types=1);

/*
 * This file is part of the twig-to-svg package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhard\TwigToSvg\Io\Info\PathString;

use PascalEberhard\TwigToSvg\Config as MyConfig;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2018 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Utils
{

    /**
     * Add directory seperator if not set
     *
     * @todo Via formatter class and formatter handler (list of formatter) or string processor
     * @param string $path
     * @param string $separator Optional, default: \PascalEberhard\TwigToSvg\MyConfig::DIRECTORY_SEPARATOR
     * @return string
     */
    public static function addDirectorySeparatorAtEnd(
        string $path,
        string $separator = MyConfig::DIRECTORY_SEPARATOR
    ): string {
        $separatorLength = mb_strlen($separator, MyConfig::CHARSET);
        if ($separator !== mb_substr($path, 0 - $separatorLength, $separatorLength, MyConfig::CHARSET)) {
            $path .= $separator;
        }

        return $path;
    }

    /**
     * Is windows path?
     *
     * @param string $path Must be absolute
     * @return bool
     * @throws \InvalidArgumentException
     */
    public static function isWindowsFormat(string $path): bool
    {
        // Must be absolute path
        if ('' == $path) {
            throw new \InvalidArgumentException('$path must not be empty');
        }
        // @todo Check is absolute path

        // Checks
        $fistChar = mb_substr($path, 0, 1, MyConfig::CHARSET);
        if ('/' == $fistChar) {
            return false;
        } elseif (!ctype_alpha($fistChar)) {
            return false;
        } elseif (0 === preg_match('/^[a-z]+:(\\\\|\/)/iu', $path)) {
            return false;
        }

        return true;
    }

    /**
     * Convert linux to windows path
     *
     * @param string $path
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function linuxToWindows(string $path): string
    {
        // Must be absolute path
        if ('' == $path) {
            throw new \InvalidArgumentException('$path must not be empty');
        }
        // @todo Check is absolute path

        // We need no convert, if $path not contains backslash
        $pos = mb_strpos($path, '/', 0, MyConfig::CHARSET);
        if (false === $pos) {
            return $path;
        }

        // Convert
        if (0 !== $pos) {
            throw new \LogicException('$path is not convert-able to windows format: ' . $path);
        }

        $letter = mb_substr($path, 1, 1, MyConfig::CHARSET);
        if (!ctype_alpha($letter)) {
            throw new \LogicException('$path is not convert-able to windows format: ' . $path);
        }

        if ('/' === mb_substr($path, 0, 1, MyConfig::CHARSET)
            && '/' === mb_substr($path, 2, 1, MyConfig::CHARSET)
        ) {
            $path = mb_strtoupper($letter, MyConfig::CHARSET) . ':' . mb_substr($path, 2, null, MyConfig::CHARSET);
            $path = preg_replace('/\//iu', '\\', $path);
        } else {
            throw new \LogicException('$path is not convert-able to windows format');
        }

        return $path;
    }

    /**
     * Convert windows to linux path
     *
     * @param string $path
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function windowsToLinux(string $path): string
    {
        // Must be absolute path
        if ('' == $path) {
            throw new \InvalidArgumentException('$path must not be empty');
        }
        // @todo Check is absolute path

        // We need no convert, if $path not contains backslash
        if (false === mb_strpos($path, '\\', 0, MyConfig::CHARSET)) {
            return $path;
        }

        // Convert
        $path = preg_replace('/\\\\/iu', '/', $path);
        if (':/' === mb_substr($path, 1, 2, MyConfig::CHARSET)) {
            $path = preg_replace_callback('/^([a-z]):\//iu', function ($match) {
                return '/' . mb_strtolower($match[1], MyConfig::CHARSET) . '/';
            }, $path);
        }

        return $path;
    }
}
