#!/usr/bin/env sh
<?php

/*
 * This file is part of the twig-to-svg package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR
    . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'
;

use PascalEberhard\TwigToSvg\Command;
use Symfony\Component\Console\Application;

$app = new Application();
$app->addCommands([
    new Command\SingleFile(),
]);

$app->run();

exit;
