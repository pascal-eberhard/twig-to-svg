# Twig to SVG

A little project to create SVGs from a twig template and config data.

The project is also thought as development and test zone for my other projects.

## Properties

```yaml
programmingLanguage: "PHP"
```

## Run

```bash
php bin/project/svgGenerator.php single.file config/singleFile.yml -vv
```

## Some shell commands

```bash
# Check syntax (A bit weird commands in the .sh file, because currently running at windows)
sh checkSyntax.sh | grep -iv "no syntax errors"

# Unit tests
bin/phpunit -vv

# Code sniff/automatic corrections
bin/phpcbf --standard=PSR2 bin
bin/phpcbf --standard=PSR2 src

# Code sniff/checks
bin/phpcs --standard=PSR2 bin
bin/phpcs --standard=PSR2 src
```

