# Change log

## Version 1.0

```yaml
checksDone:
  codeStyle: true
  syntax:
    php: true
  tests:
    unit: true
environment:
  operationSystem: "Windows 7"
  PHP:
    version: "7.2.1"
  tools:
    - "GIT bash"
```
